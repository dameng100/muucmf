<?php











namespace Composer;

use Composer\Autoload\ClassLoader;
use Composer\Semver\VersionParser;








class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'topthink/think',
  ),
  'versions' => 
  array (
    'adbario/php-dot-notation' => 
    array (
      'pretty_version' => '2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '081e2cca50c84bfeeea2e3ef9b2c8d206d80ccae',
    ),
    'aferrandini/phpqrcode' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c1c0454d43710ab5bbe19a51ad4cb41c22e3d46',
    ),
    'alibabacloud/client' => 
    array (
      'pretty_version' => '1.5.31',
      'version' => '1.5.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '19224d92fe27ab8ef501d77d4891e7660bc023c1',
    ),
    'aliyuncs/oss-sdk-php' => 
    array (
      'pretty_version' => 'v2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '572d0f8e099e8630ae7139ed3fdedb926c7a760f',
    ),
    'clagiordano/weblibs-configmanager' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c8ebcc62782313b1278afe802b120d18c07a059',
    ),
    'danielstjules/stringy' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'df24ab62d2d8213bbbe88cc36fc35a4503b4bd7e',
    ),
    'easywechat-composer/easywechat-composer' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3fc6a7ab6d3853c0f4e2922539b56cc37ef361cd',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.16.0',
      'version' => '4.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '523407fb06eb9e5f3d59889b3978d5bfe94299c8',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v6.3.1',
      'version' => '6.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ddfaddcb520488b42bca3a75e17e9dd53c3667da',
    ),
    'guzzlehttp/command' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7883359e0ecab8a8f7c43aad2fc36360a35d21e8',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.5.0',
      'version' => '7.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b50a2a1251152e43f6a37f0fa053e730a67d25ba',
    ),
    'guzzlehttp/guzzle-services' => 
    array (
      'pretty_version' => '1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4989d902dd4e0411b320e851c46f3c94d652d891',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.5.2',
      'version' => '1.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b94b2807d85443f9719887892882d0329d1e2598',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '2.4.3',
      'version' => '2.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '67c26b443f348a51926030c83481b85718457d3d',
    ),
    'guzzlehttp/uri-template' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b945d74a55a25a949158444f09ec0d3c120d69e2',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.1.10',
      'version' => '1.1.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '3239285c825c152bcc315fe0e87d6b55f5972ed1',
    ),
    'league/flysystem-cached-adapter' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd1925efb2207ac4be3ad0c40b8277175f99ffaff',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ff6248ea87a9f116e78edd6002e39e5128a0d4dd',
    ),
    'maennchen/zipstream-php' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '211e9ba1530ea5260b45d90c9ea252f56ec52729',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab8bc271e404909db09ff2d5ffa1e538085c0f22',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c66aefcafb4f6c269510e9ac46b82619a904c576',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '720488632c590286b88b80e62aa3d3d551ad4a50',
    ),
    'mtdowling/jmespath.php' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b87907a81b87bc76d19a7fb2d61e61486ee9edb',
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.8.4',
      'version' => '1.8.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a867478eae49c9f59ece437ae7f9506bfaa27483',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.63.0',
      'version' => '2.63.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ad35dd71a6a212b98e4b87e97389b6fa85f0e347',
    ),
    'overtrue/socialite' => 
    array (
      'pretty_version' => '3.5.3',
      'version' => '3.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0d1a29e853cdb01e636715fe96290ed4b62106bc',
    ),
    'overtrue/wechat' => 
    array (
      'pretty_version' => '5.31.0',
      'version' => '5.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cfd0feb6e0bee504d2f39d141e2b382d25fc441b',
    ),
    'pclzip/pclzip' => 
    array (
      'pretty_version' => '2.8.2',
      'version' => '2.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '19dd1de9d3f5fc4d7d70175b4c344dee329f45fd',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v6.6.5',
      'version' => '6.6.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '8b6386d7417526d1ea4da9edb70b8352f7543627',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.25.2',
      'version' => '1.25.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a317a09e7def49852400a4b3eca4a4b0790ceeb5',
    ),
    'pimple/pimple' => 
    array (
      'pretty_version' => 'v3.5.0',
      'version' => '3.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a94b3a4db7fb774b3d78dad2315ddc07629e1bed',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0 || 2.0.0 || 3.0.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'qcloud/cos-sdk-v5' => 
    array (
      'pretty_version' => 'v2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb9f0f1a72922413a3d8eae0f02fd7f78e7d847b',
    ),
    'qeq66/jwt' => 
    array (
      'pretty_version' => '3.3.5',
      'version' => '3.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bd2fa6c51704dc18c61026c852c789224d7190a0',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'symfony/cache' => 
    array (
      'pretty_version' => 'v5.4.15',
      'version' => '5.4.15.0',
      'aliases' => 
      array (
      ),
      'reference' => '60e87188abbacd29ccde44d69c5392a33e888e98',
    ),
    'symfony/cache-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '64be4a7acb83b6f2bf6de9a02cee6dad41277ebc',
    ),
    'symfony/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0',
      ),
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8b495ea28c1d97b5e0c121748d6f9b53d075c66',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v5.4.9',
      'version' => '5.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e6ce1cc0279e3ff3c8ff0f43813bc88d21ca1bc',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f98b54df6ad059855739db6fcbc2d36995283fe1',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0',
      ),
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v5.4.15',
      'version' => '5.4.15.0',
      'aliases' => 
      array (
      ),
      'reference' => '75bd663ff2db90141bfb733682459d5bbe9e29c3',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '869329b1e9894268a8a61dabb69153029b7a8c97',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9e8ecb5f92152187c4799efd3c96b78ccab18ff9',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.44',
      'version' => '4.4.44.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cee9cdc4f7805e2699d9fd66991a0e6df8252a2',
    ),
    'symfony/psr-http-message-bridge' => 
    array (
      'pretty_version' => 'v2.1.3',
      'version' => '2.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd444f85dddf65c7e57c58d8e5b3a4dbb593b1840',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4b426aac47d6427cc1a1d0f7e2ac724627f5966c',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v5.4.14',
      'version' => '5.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f0ed07675863aa6e3939df8b1bc879450b585cab',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v2.5.2',
      'version' => '2.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '136b19dd05cdf0709db6537d058bcab6dd6e2dbe',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v4.4.47',
      'version' => '4.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => '1069c7a3fca74578022fab6f81643248d02f8e63',
    ),
    'symfony/var-exporter' => 
    array (
      'pretty_version' => 'v5.4.10',
      'version' => '5.4.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '8fc03ee75eeece3d9be1ef47d26d79bea1afb340',
    ),
    'tencent/tls-sig-api-v2' => 
    array (
      'pretty_version' => 'v1.1',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cae9aa1f6ef04c64b451ef72cdbc004b87a800d4',
    ),
    'tencentcloud/common' => 
    array (
      'pretty_version' => '3.0.768',
      'version' => '3.0.768.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd5c34356304edf6e7133515e6e3a4d91089b7067',
    ),
    'tencentcloud/sms' => 
    array (
      'pretty_version' => '3.0.768',
      'version' => '3.0.768.0',
      'aliases' => 
      array (
      ),
      'reference' => '1273efd7049b8100c2cd5d685432545bc4226d2c',
    ),
    'thans/tp-jwt-auth' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ab5efcc0fd920df81fea2c404c34bb967ef13aba',
    ),
    'topthink/framework' => 
    array (
      'pretty_version' => 'v6.1.1',
      'version' => '6.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2cb56f3e6f3c479fe90ea5f28d38d3b5ef6c4210',
    ),
    'topthink/think' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'topthink/think-captcha' => 
    array (
      'pretty_version' => 'v3.0.8',
      'version' => '3.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '52fba122c953995bec3013c635025172491ae299',
    ),
    'topthink/think-filesystem' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cfc510520db9bcd22d8d80f51d7e415a2f470af6',
    ),
    'topthink/think-helper' => 
    array (
      'pretty_version' => 'v3.1.6',
      'version' => '3.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
    ),
    'topthink/think-image' => 
    array (
      'pretty_version' => 'v1.0.7',
      'version' => '1.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '8586cf47f117481c6d415b20f7dedf62e79d5512',
    ),
    'topthink/think-multi-app' => 
    array (
      'pretty_version' => 'v1.0.15',
      'version' => '1.0.15.0',
      'aliases' => 
      array (
      ),
      'reference' => '387e0dac059c20f92cac5da41a871e10829c1c97',
    ),
    'topthink/think-orm' => 
    array (
      'pretty_version' => 'v2.0.55',
      'version' => '2.0.55.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e1974a4c3b1b4c5b808fcc0863fc254e711dee13',
    ),
    'topthink/think-queue' => 
    array (
      'pretty_version' => 'v3.0.7',
      'version' => '3.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cded7616e313f9daa55c0ad0de5791f0d1fb3066',
    ),
    'topthink/think-template' => 
    array (
      'pretty_version' => 'v2.0.8',
      'version' => '2.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'abfc293f74f9ef5127b5c416310a01fe42e59368',
    ),
    'topthink/think-trace' => 
    array (
      'pretty_version' => 'v1.5',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '55027fd79abb744f32a3be8d9e1ccf873a3ca9b7',
    ),
    'topthink/think-view' => 
    array (
      'pretty_version' => 'v1.0.14',
      'version' => '1.0.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edce0ae2c9551ab65f9e94a222604b0dead3576d',
    ),
    'topthink/think-worker' => 
    array (
      'pretty_version' => 'v3.0.7',
      'version' => '3.0.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ba54333f337ee2c3f06054bb896bba9696d926c',
    ),
    'wechatpay/wechatpay' => 
    array (
      'pretty_version' => '1.4.6',
      'version' => '1.4.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edbdb6bb19e0818b0576043b265ff1b1e188d668',
    ),
    'workerman/crontab' => 
    array (
      'pretty_version' => 'v1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '28106241415049ee340a8a7cd9b640165240a2fa',
    ),
    'workerman/gateway-worker' => 
    array (
      'pretty_version' => 'v3.0.22',
      'version' => '3.0.22.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a615036c482d11f68b693998575e804752ef9068',
    ),
    'workerman/workerman' => 
    array (
      'pretty_version' => 'v3.5.32',
      'version' => '3.5.32.0',
      'aliases' => 
      array (
      ),
      'reference' => '10b24f0aaed93e5a29f3fdd68f3334b5b142c4bc',
    ),
  ),
);
private static $canGetVendors;
private static $installedByVendor = array();







public static function getInstalledPackages()
{
$packages = array();
foreach (self::getInstalled() as $installed) {
$packages[] = array_keys($installed['versions']);
}

if (1 === \count($packages)) {
return $packages[0];
}

return array_keys(array_flip(\call_user_func_array('array_merge', $packages)));
}









public static function isInstalled($packageName)
{
foreach (self::getInstalled() as $installed) {
if (isset($installed['versions'][$packageName])) {
return true;
}
}

return false;
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

$ranges = array();
if (isset($installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = $installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['version'])) {
return null;
}

return $installed['versions'][$packageName]['version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getPrettyVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return $installed['versions'][$packageName]['pretty_version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getReference($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['reference'])) {
return null;
}

return $installed['versions'][$packageName]['reference'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getRootPackage()
{
$installed = self::getInstalled();

return $installed[0]['root'];
}








public static function getRawData()
{
@trigger_error('getRawData only returns the first dataset loaded, which may not be what you expect. Use getAllRawData() instead which returns all datasets for all autoloaders present in the process.', E_USER_DEPRECATED);

return self::$installed;
}







public static function getAllRawData()
{
return self::getInstalled();
}



















public static function reload($data)
{
self::$installed = $data;
self::$installedByVendor = array();
}





private static function getInstalled()
{
if (null === self::$canGetVendors) {
self::$canGetVendors = method_exists('Composer\Autoload\ClassLoader', 'getRegisteredLoaders');
}

$installed = array();

if (self::$canGetVendors) {
foreach (ClassLoader::getRegisteredLoaders() as $vendorDir => $loader) {
if (isset(self::$installedByVendor[$vendorDir])) {
$installed[] = self::$installedByVendor[$vendorDir];
} elseif (is_file($vendorDir.'/composer/installed.php')) {
$installed[] = self::$installedByVendor[$vendorDir] = require $vendorDir.'/composer/installed.php';
}
}
}

$installed[] = self::$installed;

return $installed;
}
}
